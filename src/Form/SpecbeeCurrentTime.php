<?php

namespace Drupal\specbee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SpecbeeCurrentTime.
 */
class SpecbeeCurrentTime extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'specbee.specbeecurrenttime',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'specbee_current_time';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('specbee.specbeecurrenttime');
    $form['country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('country'),
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('city'),
    ];
    $form['timezone'] = [
      '#type' => 'select',
      '#title' => $this->t('Timezone'),
      '#description' => $this->t('Please select timezone'),
      '#options' => ['America/Chicago' => $this->t('America/Chicago'),
                        'America/New_York' => $this->t('America/New_York'),
                        'Asia/Tokyo' => $this->t('Asia/Tokyo'),
                        'Asia/Dubai' => $this->t('Asia/Dubai'),
                        'Asia/Kolkata' => $this->t('Asia/Kolkata'),
                        'Europe/Amsterdam' => $this->t('Europe/Amsterdam'),
                        'Europe/Oslo' => $this->t('Europe/Oslo'),
                        ],
      '#size' => 1,
      '#default_value' => $config->get('timezone'),
    ];
    $form['#cache'] = ['max-age' => 0];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('specbee.specbeecurrenttime')
      ->set('country', $form_state->getValue('country'))
      ->set('city', $form_state->getValue('city'))
      ->set('timezone', $form_state->getValue('timezone'))
      ->save();
  }

}
