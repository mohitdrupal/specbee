<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Drupal\specbee;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Description of GetCurrentTime
 *
 * @author Mohit Gupta
 */
class GetCurrentTime {
    //put your code here
    protected $country;
    protected $city;
    protected $timezone;
    protected $currentTime;
    protected $timeFormat = "d M Y -  g:i A";




    public function __construct(ConfigFactory $config_factory) {
       $this->country = $config_factory->get('specbee.specbeecurrenttime')
               ->get('country');
       $this->city = $config_factory->get('specbee.specbeecurrenttime')
               ->get('city');
       $this->timezone = $config_factory->get('specbee.specbeecurrenttime')
               ->get('timezone');       
    }
    
  
    
    /**
     * @name getCurrentTime
     * @return type string
     */
    public function getCurrentTime(){
        $this->country;
        $this->city;
        $this->timezone;        
        
        $timestamp = \Drupal::time()->getRequestTime();
        $date = DrupalDateTime::createFromTimestamp($timestamp, $this->timezone);
        $this->currentTime = $date->format($this->timeFormat);
        $data['city'] =  $this->city;
        $data['country'] =  $this->country;
        $data['currentTime'] =  $this->currentTime;
       return $data; 
    }
}
