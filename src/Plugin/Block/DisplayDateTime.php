<?php

namespace Drupal\specbee\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\specbee\GetCurrentTime;

/**
 * Provides a 'DisplayDateTime' block.
 *
 * @Block(
 *  id = "display_date_time",
 *  admin_label = @Translation("Display date time"),
 * )
 */
class DisplayDateTime extends BlockBase implements ContainerFactoryPluginInterface {

    protected $GetCurrentTime;


    public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,GetCurrentTime $GetCurrentTime) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->GetCurrentTime = $GetCurrentTime;
  }

   public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
       return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('specbee.getcurrentime')
    );     
   }

  
    
  /**
   * {@inheritdoc}
   */
  public function build() {
       \Drupal::service('page_cache_kill_switch')->trigger();
    $result =  $this->GetCurrentTime->getCurrentTime();    
    return [
      '#theme' => 'display_date_time',      
      '#content' => $result,
        '#cache' => [
            'max-age' => 0,
          ]
    ];    
  }
  
  public function getCacheMaxAge() {
    return 0;
}

}
